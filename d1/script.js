// Javascript ES6 Updates

// 1. Exponent Operator 
// Pre version
const firstNum = Math.pow(8, 2);
console.log(firstNum)

// ES6
const secondNum = 8 ** 2;
console.log(secondNum)

// 2. Template literals
// allows to write strings without using the concatenation operator(+)
// Greatly helps with code readability

// Pre-Template Literal String
// Uses single quotes('') hassle

let name = 'Smith';
// String Using template literals
// Uses backticks (``)
message = `Hello ${name}! Welcome to Programming!`;
console.log(`Message with template literals: ${message}`) 

// Multi-line using template literals
const anotherMessage =  `
${name} attended a math competition.
He won it by solving the problem 8 ** 2
with solution of ${secondNum}.`

console.log(anotherMessage)

// JS expressions (${}) We can put solution inside

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`)
// 3. ARRAY DESTRUCTURING
// unpack elements in arrays into distinct variables
// name array elements with variables instead of using index numbers. Helps with code readability.
// Syntax: let/const [variableName,variablename] = array

// Pre version
const fullName = ['Juan','Dela','Cruz'];
// Array Destructuring
const [firstName, middleName, lastName] = fullName
console.log(firstName)
console.log(middleName)
console.log(lastName)

// expressions are any valid unit of code that resolves to a value
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you`)

// 4. OBJECT DESTRUCTURING
// unpack elements in objects into distinct variables.

const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
};
// Shortens the syntax for accessing properties from objects
// Syntax: let/const {propertyName,propertyName} = objectName

const { givenName, maidenName, familyName} = person;
console.log(givenName)
console.log(maidenName)
console.log(familyName)

console.log(`Hello ${givenName} ${maidenName} ${familyName}! IT's good to see you again.`)

function getFullName({ givenName, maidenName, familyName }){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}
getFullName(person)

//5. ARROW FUNCTIONS

// Normal function
function printFullName(fName, mName, lName){
	console.log(fName + ' ' + mName + ' ' + lName)
}
// function declaration
// function name
// parameters = placeholder? the name of an argument to be passed to the function
// statements = function body
// invoke/call back function

printFullName('John','D.','Smith')

// ES6 ARROW FUNCTION
// compact alternative syntax to traditional functions
// useful for code snippets where creating functions will not be reused in any other portion of the code
// as anonymous function
// adheres to the 'DRY' principle (Don't repeat yourself) where there's no longer need to create a function and think of a name for functions that will only be used in certain snippets
const variableName = () => {
	console.log('Hello World')
}
/*
Convert this to arrow function
function printFullName(fName, mName, lName){
	console.log(fName + ' ' + mName + ' ' + lName)
}*/
// conversion to arrow function
const printFName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lName}`)
}
printFName('Jane','D.','Smith')

// ArrowFunctions with Loops
// Pre-Arrow function

const students = ['john','jane','joe'];

students.forEach(function(student){
	console.log(`${student} is a student`)
})
// Arrow Function
students.forEach((student) => console.log(`${student} is a student.`));

// 6. Arrow function with IMPLICIT RETURN STATEMENT
// There are instances when you can omit the return statement. This works because even without the return statement, JS implicitly adds it for the result of a function

// Pre-arrow Function
/*const add = (x, y) => {
	return x + y;
}
let total = add(1, 2);
console.log(total)*/

// Arrow Function
const add = (x, y) => x + y

let total = add(1, 2);
console.log(total)

// omit () if there's only one parameter
// let filterFriends = friends.filter(friend => friend.length ===4)

// 7. Arrow FUnction DEFAULT ARGUMENT VALUE
// Provides a default argument value if none is provided when the function is invoked.

const greet = (name = 'User') => {
	return `Good morning, ${name}`
} 
console.log(greet())

// 8. CREATING A CLASS
/*
SYNTAX:
	class className {
		constructor(objectPropertyA,objectPropertyB){
			this.objectPropertyA = objectPropertyA;
			objectPropertyB = objectPropertyB
		}
	}

= The constructor is a special method of a class for creating/initializing an object for that class.
= The 'this' keyword refers to the properties of an object created from the class.
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
const myCar = new Car();
console.log(myCar) //undefined

// Value of properties may be assigned after creation/instantiation of an object
myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;

console.log(myCar)

// Creating/instantiating a new object from the car class with initialized values
const myNewCar = new Car('Toyota','Vios',2021)
console.log(myNewCar)

// 9. TERNARY OPERATOR
// conditional operator
//  It has 3 operands
// condition, followed by question mark ?, then and expression to execute if the conditionis truth followed by colon : and finally the expression to execute if the condition is false

if(condition == 0){
	return true
}else{
	false
}
// same as : Ternary Operator - it is inside the object
(condition == 0) ? true : false