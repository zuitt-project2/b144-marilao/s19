/*3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)*/

const getCube = 2 ** 3;
// console.log(getCube)
/*
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…*/
console.log(`The cube of 5 is ${getCube} `)

/*5. Create a variable address with a value of an array containing details of an address.*/

let address = ['258 Washington Ave NW','California','90011'];

/*6. Destructure the array and print out a message with the full address using Template Literals.*/
const [avenue, country, postalCode] = address
console.log(`I live at ${avenue},${country} ${postalCode}`)

/*7. Create a variable animal with a value of an object data type with different animal details as it’s properties.*/
let animal = {
	name: 'Lolong',
	kind: "crocodile",
	weight: 1075,
	
	
	};


/*8. Destructure the object and print out a message with the details of the animal using Template Literals.*/
const {name, kind, weight, measurement} = animal

console.log(`${name} was a saltwater ${kind}. He weighed at ${weight} with a measurement of 20 ft 3 in.`)

/*9. Create an array of numbers.*/

const numbers = [1,2,3,4,5];

/*10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

numbers.forEach((number) =>console.log(`${number}`));
	
/*11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.*/

const add = (a,b,c,d,e) => a + b + c + d + e

let reduceNumber = add(1,2,3,4,5);
console.log(reduceNumber)

/*12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.*/
class dog {
	constructor(name, age, breed){
		this.name = 'Frankie' ;
		this.age = 5 ;
		this.breed = 'Miniature Dachshund' ;
	}
}

const myDog = new dog();
console.log(myDog)
